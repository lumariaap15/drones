<?php

use App\Models\Dron;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    $drones = Dron::all();
    return view('dashboard', compact('drones'));
})->middleware(['auth'])->name('dashboard');

Route::post('/dron/save', function (Request $request) {
    $dron = new Dron();
    $dron->peso = $request->peso;
    $dron->alto = $request->alto;
    $dron->ancho = $request->ancho;
    $dron->largo = $request->largo;
    $dron->motores = $request->motores;
    $dron->velocidad_maxima = $request->velocidad_maxima;
    $dron->camara = !!$request->has('camara');
    $dron->gps = !!$request->has('gps');
    $dron->save();
    return redirect()->back();
})->middleware(['auth'])->name('dron.save');

require __DIR__.'/auth.php';
