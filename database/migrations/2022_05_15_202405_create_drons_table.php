<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drons', function (Blueprint $table) {
            $table->id();
            $table->string('peso');
            $table->string('largo');
            $table->string('alto');
            $table->string('ancho');
            $table->boolean('camara');
            $table->boolean('gps');
            $table->string('motores');
            $table->string('velocidad_maxima');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drons');
    }
};
